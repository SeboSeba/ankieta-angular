import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Vote } from './vote';
import { switchMap, filter, flatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  private baseURL: string = environment.MLAB_BASE_URL;
  private apiKey: string = environment.MLAB_API_KEY;


  constructor(private _http: HttpClient) { }

  getVotes(): Observable<Vote[]> {
    console.log('getVotes Start');
    return this._http.get<Vote[]>(this.baseURL + 'databases/prosta-ankieta/collections/results?apiKey=' + this.apiKey);
  }

  // getVotes2(): Observable<Vote[]> {
  //   return this._http.get<Vote[]>(this.baseURL + 'databases/prosta-ankieta/collections/results?apiKey=' + this.apiKey)
  //     .pipe(
  //       map( results => results.filter( r => r.count < 20) )
  //     )
  // }

  // getVotes3(): Observable<Vote[]> {
  //   return this._http.get<Vote[]>(this.baseURL + 'databases/prosta-ankieta/collections/results?apiKey=' + this.apiKey);
  // }

  // putVote(choose: string): Observable<Vote>{

  //   let oldBmwVotes: Vote;
  //   let newBmwVotes: Vote;
  //   let updatedVote: Vote;

  //   return this.getVotes().pipe(switchMap(
  //     result => {
  //       console.log('serwis', result);

  //       oldBmwVotes = result[0];
  //       newBmwVotes = result[1];

  //       if(choose === 'old'){
  //         updatedVote = oldBmwVotes;
  //       } else if(choose === 'new'){
  //         updatedVote = newBmwVotes;
  //       }
  //       updatedVote.count += 1;
  //       console.log(updatedVote._id.$oid, 'new');

  //       return this._http.put<Vote>(this.baseURL + 'databases/prosta-ankieta/collections/results/'+ updatedVote._id.$oid + '?apiKey=' + this.apiKey, updatedVote);
  //     }
  //   )
  // }

  putVote(choose: string): Observable<Vote>{
    console.log('puVote Start');

    let voteToUpdate: Vote;

    return this.getVotes().pipe(
      map( results => results.filter( r => r.name === choose) ),
      switchMap(
      result => {

        console.log('serwis', result);

        voteToUpdate = result[0];

        voteToUpdate.count += 1;
        console.log(voteToUpdate, 'new');

        return this._http.put<Vote>(this.baseURL + 'databases/prosta-ankieta/collections/results/'+ voteToUpdate._id.$oid + '?apiKey=' + this.apiKey, voteToUpdate);
      }
    ))
  }

}
