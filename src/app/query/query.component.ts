import { Component, OnInit } from '@angular/core';
import { QueryService } from '../query.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {

  loading: boolean = false;

  constructor(
    private queryService: QueryService,
    private router: Router
  ) { }

  ngOnInit() {
  }

 vote(choose: string){
   this.loading = true;
   console.log('click');
   console.log(choose);
   this.queryService.putVote(choose).subscribe(
     result => {
      console.log('query component', result);
      this.router.navigate(['/results']);
      console.log('kobncze robote');
      this.loading = false;
     }
   );
 }

}
